# Base Eclipse project for Spring-Boot + Gradle + Swagger2.0 (springfox)
## System Requirements and Prerequisites
1. Install Java SDK
	
	`brew install Caskroom/cask/java`
2. Install Eclipse J2EE

	`brew install Caskroom/cask/eclipse-jee`
3. Install Eclipse Gradle plugin

	Eclipse > Help > Eclipse Marketplace > search for Gradle and install the plugin
4. Install Gradle

	`brew install gradle`

## Start building the base project
1. Get base project template from git
   
    `git clone git@gitlab.com:heha/spring-boot-base-project.git`
2. Build Eclipse project files


	`gradle eclipse`
3. Open Eclipse and import the project as Gradle project (browse the folder and click Build Model)
4. Remember to change the name of SampleApplication, edit the ApiInfo's parameters (line 28 - 30 in SampleApplication.java) and change the name of jar (line 15 in build.gradle) 
5. The folder src folder structure can refer to [http://gradle.org/docs/current/userguide/java_plugin.html](http://gradle.org/docs/current/userguide/java_plugin.html)
5. Right click on build.gradle > Run as gradle build Gradle Build to build the project
6. Click Run and select the main class to start the project

	Swagger api document json: http://localhost:8080/v2/api-docs