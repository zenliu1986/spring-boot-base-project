package com.heha.base.exception;

public class BaseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5474612851387762717L;
	
	private ExceptionInfo info = null;
	public BaseException(ExceptionInfo info){
		this.info = info;
	}
	public ExceptionInfo getInfo() {
		return info;
	}
	public void setInfo(ExceptionInfo info) {
		this.info = info;
	}
}
