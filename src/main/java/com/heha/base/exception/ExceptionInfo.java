package com.heha.base.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value="error")
public class ExceptionInfo {
	private String message = "";
	private String type = "";
	private String code = "";
	
	@JsonInclude(Include.NON_NULL)
	private String[] fields = null;
	@JsonIgnore
	private int httpStatus;
	
	public ExceptionInfo(){
		
	}
	
	public ExceptionInfo(String message, String type, String code, int httpStatus, String[] fields){
		this.message = message;
		this.type = type;
		this.code = code;
		this.httpStatus = httpStatus;
		this.fields = fields;					
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String[] getFields() {
		return fields;
	}
	public void setFields(String[] fields) {
		this.fields = fields;
	}
	public int getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}
}
