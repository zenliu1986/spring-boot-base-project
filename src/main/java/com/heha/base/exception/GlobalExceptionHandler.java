package com.heha.base.exception;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler implements ErrorController{//extends ResponseEntityExceptionHandler{
	
	//for handle all exceptions
	@ExceptionHandler(Exception.class)
	@ResponseBody
	protected ResponseEntity<ExceptionInfo> exceptionHandle(HttpServletRequest req, Exception exception, HttpSession session, HttpServletResponse response, Principal principal, WebRequest request){
		ExceptionInfo info;
		HttpStatus httpStatus;
		if (exception instanceof BaseException){
			//customized exception, will contain ExceptionInfo
			info = ((BaseException) exception).getInfo();
			
			try{
				httpStatus = HttpStatus.valueOf(info.getHttpStatus());
			}catch(IllegalArgumentException e){
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}else{
			//runtime or other non-customized exception
			info = new ExceptionInfo();
			info.setType("");
			info.setCode("500");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			info.setMessage(exception.getMessage());
		}
        
		//used to return the result object with http status code 
		ResponseEntity<ExceptionInfo> res = new ResponseEntity<ExceptionInfo>(info, httpStatus);
		return res;
	}
	
	//for 404 exception
	@RequestMapping(value="/error")
	@ResponseBody
	protected ResponseEntity<ExceptionInfo> error404Handle(){
		ExceptionInfo info = new ExceptionInfo();
		info.setType("");
		info.setCode("404");
		info.setHttpStatus(HttpStatus.NOT_FOUND.value());
		info.setMessage("Invalid path");
		
		//used to return the result object with http status code 
		ResponseEntity<ExceptionInfo> res = new ResponseEntity<ExceptionInfo>(info, HttpStatus.NOT_FOUND);
		return res;
	}

	
	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return "/error";
	}
	
	/*@ExceptionHandler(Exception.class)
	//@ResponseBody
	protected ResponseEntity<Object> handleException(HttpServletRequest req, Exception exception, HttpSession session, HttpServletResponse response, Principal principal, WebRequest request){
		ExceptionInfo info = new ExceptionInfo();
		info.setCode("");
		info.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
		return handleExceptionInternal(exception, info, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
	}*/

}
