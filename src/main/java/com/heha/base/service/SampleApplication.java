package com.heha.base.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan("com.heha")
public class SampleApplication {
	
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SampleApplication.class, args);

        System.out.println("Server started from Main.main()!");
	}
	
	
	@Bean
	public Docket testApi(){
		//configuration of Swagger, use Docket object to apply the configuration
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(new ApiInfo(
            "<SERVICE_NAME>",
            "<SERVICE_DESC>",
            "<SERVICE_VERSION>",
            null,
            null,
            null,
            null
        ))    
        .select()   
        .build();		
	}
}
